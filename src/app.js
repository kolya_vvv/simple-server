import dotenv from "dotenv";
dotenv.config();
import express from "express";
import cookieParser from "cookie-parser";
import logger from "morgan";
import mongoose from "mongoose";
import cors from "cors";
import bodyParser from "body-parser";

mongoose.connect(process.env.DB_URL,{useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
import indexRouter from "./routes/index.js";

const app = express();

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use("/", indexRouter);

export default app;
