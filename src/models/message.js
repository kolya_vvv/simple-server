import mongoose from "mongoose";

const messageSchema = mongoose.Schema({
    author:{
        type: String,
        required: true
    },
    text:{
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now()
    }
})

const message = mongoose.model("message",messageSchema);

export default message