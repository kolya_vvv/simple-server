import mongoose from "mongoose";
const ObjectId = mongoose.Schema.Types.ObjectId;

const orderSchema = mongoose.Schema({
    user: {
        type: ObjectId,
        ref: "user",
        required: true
    },
    product: {
        type: ObjectId,
        ref: "product",
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        enum: ["new", "delivery", "paid"],
        default: "new"
    }
})
const order = mongoose.model("Order",orderSchema);
export default order;