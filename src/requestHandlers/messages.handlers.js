import Messages from "../models/message.js";

export async function getAllMessages(req,res,next) {
	try {
		let messages = await Messages.find();
		res.status(200).json({
			count: messages.length,
			allMessages: messages.map(m=>{
				return {
					_id: m._id,
					author: m.author,
					text: m.text.substr(0,50) + "...",
					date: m.date,
					request: {
						url: "http://localhost:3000/messages/" + m._id
					}
				}
			})
		});
	}catch (e) {
		next(e);
	}
}
export async function getMessageById(req,res,next) {
	try {
		let {messageId} = req.params;
		let message = await Messages.findById(messageId);
		res.status(200).json({
			message: message,
			request: {
				url: "http://localhost:3000/messages/"
			}
		});
	}catch (e) {
		next(e);
	}
}
export async function addNewMassage(req,res,next) {
	try {
		let {text} = req.body;
		const message = new Messages({text});
		let result = await message.save();
		res.status(200).json({
			message: "Created message successfully",
			createdMessage: result,
			request: {
				url: "http://localhost:3000/messages/" + result._id
			}
		})
	}catch (e) {
		next(e);
	}
}
export async function updateMessageById(req,res,next) {
	try {
		let {messageId} = req.params;
		let {text} = req.body;
		let result = await Messages.updateOne({_id: messageId},{text});
		res.status(200)({
			message: "Message updated",
			request:{
				url: "http://localhost:3000/messages/" + result._id
			}
		})
	}catch (e) {
		next(e);
	}
}
export async function deleteMessageById(req,res,next) {
	try{
		let {messageId} = req.params;
		let result = await Messages.deleteOne({_id: messageId});
		res.status(200).json({
			message: "message deleted",
			request:{
				url: "http://localhost:3000/messages/"
			}
		});
	}catch (e) {
		next(e);
	}
}