
import Order from "../models/order.js"

export async function addNewOrder(req,res,next) {
    try {
        const {user, product, quantity} = req.body;
        let order = Order({product,user,quantity});
        let result = await order.save();
        res.status(200).json({
            message: "create order successfully",
            order: reult,
            request: {
                url: "http://localhost:3000/orders/" + result._id
            }
        })
    }catch (e) {
        next(e)
    }
}

export async function getAllOrders(req,res,next) {
    try {
        let orders = await Order.find();
        res.status(200).json({
            count: orders.length,
            orders: orders.map(o=>{
                return {
                    _id: o._id,
                    user: o.user,
                    product: o.product,
                    quantity: o.quantity,
                    status: o.status,
                    request: {
                        url: "http://localhost:3000/orders/" + o._id
                    }
                }
            })
        })
    }catch (e) {
        next(e);
    }
}
export async function getOrderById(req,res,next) {
    try {
        let {orderId} = req.params;
        let order = await Order.findById({_id: orderId});
        res.status(200).json({
            order: order,
            request: {
                url: "http://localhost:3000/orders/"
            }
        });
    }catch (e) {
        next(e);
    }
}
export async function getOrdersByStatus(req,res,next) {
    try {
        let {status} = req.params;
        let orders = await Order.find({status});
        res.status(200).json({
            count: orders.length,
            orders: orders.map(o=>{
                return {
                    _id: o._id,
                    user: o.user,
                    product: o.product,
                    quantity: o.quantity,
                    status: o.status,
                    request: {
                        url: "http://localhost:3000/orders/" + o._id
                    }
                }
            })
        })
    }catch (e) {
        next(e);
    }
}
export async function orderPayment(req,res,next){
    try {
        let {_id} = req.params;
        let order = await Order.findById(_id)
        if(order.status === "delivery"){
            order.status = "paid";
            let result = await order.save();
            res.status(200).json({
                message: "order payment successfully",
                request: {
                    url: "http://localhost:3000/orders/" + result._id
                }
            })
        }else {
            throw new Error("Product status is not 'delivery'. Payment not possible")
        }
    }catch (e) {
        next(e);
    }
}
export async function orderDelivery(req,res,next) {
    try {
        let {_id} = req.params;
        let order = await Order.findById(_id);
        if(order.status === "new"){
            order.status = "delivery";
            let result = await order.save();
            res.status(200).json({
                message: "order delivery successfully",
                request: {
                    url: "http://localhost:3000/orders/" + result._id
                }
            })
        }else {
            throw new Error("Product status is not 'new'. Delivery not possible")
        }
    } catch (e) {
        next(e);
    }
}