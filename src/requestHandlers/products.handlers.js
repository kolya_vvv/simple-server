import Product from "../models/product.js";
import Order from "../models/order.js";
import Response from "../utils/Response.js";


export async function getAllProducts(req,res,next) {
    try{
        let products = await Product.find();
        let message = "all products"
        res.status(200).json(new Response(message, products))
    }catch (e) {
        next(e)
    }
}

export async function getProductsById(req,res,next) {
    try {
        let {productId} = req.params;
        let product = await Product.findById(productId);
        const message = "Product by id"
        res.status(200).json(new Response(message, product));
    }catch (e) {
        next(e)
    }
}

export async function addNewProduct(req,res,next) {
    try {
        const {name, price, quantity} = req.body;
        let {path: img} = req.file;
        console.log({name,price,quantity,img})
        let product = Product({name,price,quantity,img});
        let result = await product.save();
        const message = "created product successfully";
        res.status(200).json(new Response(message, result))
    }catch (e) {
        next(e)
    }
}
export async function updateProductById(req,res,next) {
    try {
        let {productId} = req.params;

        let newFields = {};
        const {name, price, quantity, img} = req.body;
        if(name){newFields.name = name;}
        if(price){newFields.price = price;}
        if(quantity){newFields.quantity = quantity;}
        if(img){newFields.img = img;}
        let result = await Product.updateOne({_id: productId},newFields);
        const message = "product update";
        res.status(200).json(new Response(message, result));
    }catch (e) {
        next(e);
    }
}

export async function deleteProductById(req,res,next) {
    try{
        let {productId} = req.params;
        let result = await Product.deleteOne({_id: productId})
        const message = "product deleted";
        res.status(200).json(new Response(message, result));
    }catch (e) {
        next(e);
    }
}
export async function getAllProductOrders(req,res,next) {
    try{
        let {productId} = req.params;
        let orders = await Order.findById(productId);
        const message = "all products orders";
        if(orders){
            res.status(200).json(new Response(message, orders));
        }else {
            res.status(200).json({
                message: "no orders for this product"
            })
        }
    }catch (e) {
        next(e);
    }
}