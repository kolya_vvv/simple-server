import User from "../models/user.js";
import Order from "../models/order.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export async function getAllUsers(req,res,next) {
	try {
		let users = await User.find();
		res.status(200).json({
			count: users.length,
			users: users.map(u=>{
				return {
					_id: u._id,
					name: u.name,
					age: u.age,
					email: u.email,
					password: u.password,
					request: {
						url: "http://localhost:3000/users/" + u._id
					}
				}
			})
		});
	}catch (e) {
		next(e);
	}
}
export async function getUserById(req,res,next) {
	try {
		let {userId} = req.params;
		let user = await User.findById(userId);
		res.status(200).json({
			user: user,
			request: {
				url: "http://localhost:3000/users/"
			}
		});
	}catch (e) {
		next(e);
	}
}

export async function updateUserById(req,res,next) {
	try {
		let {userId} = req.params;
		let result = await User.updateOne({_id: userId},req.body);
		res.status(200).json({
			message: "user update",
			request: {
				url: "http://localhost:3000/users/" + userId
			}
		});
	}catch (e) {
		next(e);
	}
}
export async function deleteUserById(req,res,next) {
	try {
		let {userId} = req.params;
		let result = await User.deleteOne({_id: userId});
		res.status(200).json({
			message: "user deleted",
			request: {
				url: "http://localhost:3000/users/"
			}
		})
	}catch (e) {
		next(e);
	}
}
export async function getAllUserOrders(req,res,next) {
	try {
		let {userId} = req.params;
		let orders = await Order.find({userId})
		res.status(200).json({
			message: "all user orders",
			orders: orders.map(o=>{
				return {
					_id: o._id,
					product: o.product,
					quantity: o.quantity,
					user: o.user,
					status: o.status,
					request: {
						url: "http://localhost:3000/orders/" + o._id
					}
				}
			})
		});
	}catch (e) {
		next(e);
	}
}
export async function userSignUp(req, res, next) {
	try {
		const {name, age, email} = req.body;
		let password = await bcrypt.hash(req.body.password,10);
		const user = await User.find({email: req.body.email});
		if(user.length >= 1){
			res.status(409).json({
				message: "User with this email exists"
			})
		}else {
			const user = new User({name, age, email, password});
			const result = await user.save();
			res.status(200).json({
				message: "user created successfully",
				user: {
					_id: result._id,
					name: result.name,
					age: result.age,
					email: result.email
				},
				request: {
					url: "http://localhost:3000/users/" + result._id
				}
			})
		}
	}catch (e) {
		next(e)
	}
}

export async function userLogIn(req, res, next) {
	try {
		const {email} = req.body;
		const user = await User.find({email})
		if(user.length < 1){
			return res.status(401).json({
				message: "Auth failed"
			})
		}
		let result = await bcrypt.compare(req.body.password, user[0].password)
		if(result){
			const token = jwt.sign({
				email: user[0].email,
				userId: user[0]._id
			},process.env.JWT_KEY,{expiresIn: "1h"})
			res.status(200).json({
				message: "Auth successful",
				token
			})
		}else {
			res.status(401).json({
				message: "Auth failed"
			})
		}
	}catch (e) {
		next(e)
	}
}