import express from "express";
import usersRouter from "./users.js";
import messagesRouter from "./messages.js";
import productsRouter from "./products.js";
import ordersRouter from "./orders.js";

const router = express.Router();

router.get("/", function(req, res, next) {
	res.send("home");
});

router.use("/users",usersRouter);
router.use("/messages",messagesRouter);
router.use("/products",productsRouter);
router.use("/orders",ordersRouter);

router.use("/*",(req,res,next)=>{
	res.status(404).send("not-found");
});
router.use((err,req,res,next)=>{
	if(!err.statusCode) err.statusCode = 500;

	return res.status(err.statusCode)
		.json({error: err.toString()});
});

export default router;
