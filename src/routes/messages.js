import express from "express";
import {getAllMessages,getMessageById,addNewMassage,updateMessageById,deleteMessageById} from "../requestHandlers/messages.handlers.js";

const router = express.Router();

router.get("/",getAllMessages);
router.get("/:messageId",getMessageById);
router.post("/",addNewMassage);
router.put("/:messageId",updateMessageById);
router.delete("/:messageId",deleteMessageById);

export default router;