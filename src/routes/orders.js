import express from "express";
import {addNewOrder,getAllOrders,getOrderById,getOrdersByStatus,orderPayment,orderDelivery} from "../requestHandlers/order.handlers.js";
import checkAuth from "../middleware/check-auth.js"

const router = express.Router();

router.get("/",checkAuth,getAllOrders);
router.get("/byId/:orderId",checkAuth,getOrderById);
router.get("/byStatus/:status",checkAuth,getOrdersByStatus);
router.post("/",checkAuth,addNewOrder);
router.put("/payment/:_id",checkAuth,orderPayment);
router.put("/delivery/:_id",checkAuth,orderDelivery);

export default router;