import express from "express";
import {getAllProducts,getProductsById,addNewProduct,updateProductById,deleteProductById,getAllProductOrders} from "../requestHandlers/products.handlers.js";
import multer from "multer";
import checkAuth from "../middleware/check-auth.js"

const storage = multer.diskStorage({
    destination: (req, file, cb)=>{
        cb(null, "./uploads")
    },
    filename: (req, file, cb)=>{
        console.log(file)
        cb(null, new Date().toDateString() + file.name)
    }
})
const upload = multer({storage})
const router = express.Router();

router.get("/",getAllProducts);
router.get("/:productId",getProductsById);
router.post("/", upload.single("productImage"), addNewProduct);
router.put("/:productId",updateProductById);
router.delete("/:productId",deleteProductById);
router.get("/orders/:productId",getAllProductOrders);


export default router;