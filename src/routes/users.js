import express from "express";
import {getAllUsers,getUserById,updateUserById,deleteUserById,getAllUserOrders,userSignUp,userLogIn} from "../requestHandlers/users.handlers.js";

const router = express.Router();


router.get("/",getAllUsers);
router.get("/:userId",getUserById);
router.put("/:userId",updateUserById);
router.delete("/:userId",deleteUserById);
router.get("/orders/:userId",getAllUserOrders);


router.post("/signup",userSignUp);
router.post("/login",userLogIn);

export default router;