export default class BadRequestError extends Error{
	constructor(err) {
		super(err.message);
		this.data = {err};
		this.statusCode = 400;
	}
}
